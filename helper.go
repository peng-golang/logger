package logger

import (
	"go.uber.org/zap"
)

func AddData(data map[string]interface{}) zap.Field {
	return zap.Any("data", data)
}

func SetTraceId(id string) zap.Field {
	return zap.String("traceID", id)
}

func SetInfo(info any) zap.Field {
	return zap.Any("info", info)
}

func AddError(err error) zap.Field {
	return zap.Error(err)
}
