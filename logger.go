package logger

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"os"
)

var CommonLoggerName = "common.log"

type InitOption struct {
	ConsoleLog bool
	Level      zapcore.Level
}

func InitConsoleLog() *zap.Logger {
	encoder := GetConsoleEncoder()
	writer := zapcore.AddSync(os.Stdout)
	core := zapcore.NewCore(encoder, writer, zapcore.DebugLevel)
	return zap.New(core)
}

func InitLogger(logDir string, option InitOption) *zap.Logger {
	writeSyncer := getLogWriter(logDir, option)
	encoder := GetConsoleEncoder()
	core := zapcore.NewCore(encoder, writeSyncer, option.Level)
	return zap.New(core)
}

func getLogWriter(logDir string, option InitOption) zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   fmt.Sprintf("%s/%s", logDir, CommonLoggerName),
		MaxSize:    5,
		MaxBackups: 5,
		MaxAge:     10,
		Compress:   false,
	}
	if option.ConsoleLog {
		return zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(lumberJackLogger))
	}
	return zapcore.AddSync(lumberJackLogger)
}

func GetSugarWithType(logDir string, category string) *zap.SugaredLogger {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   fmt.Sprintf("%s/%s.log", logDir, category),
		MaxSize:    5,
		MaxBackups: 5,
		MaxAge:     10,
		Compress:   false,
	}
	writeSyncer := zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(lumberJackLogger))
	encoder := GetJSONEncoder()
	core := zapcore.NewCore(encoder, writeSyncer, zapcore.InfoLevel)
	return zap.New(core).Sugar()
}
