package logger

type ILogger interface {
	Debugw(msg string, keysAndValues ...interface{})

	Infow(msg string, keysAndValues ...interface{})

	Warnw(msg string, keysAndValues ...interface{})

	Errorw(msg string, keysAndValues ...interface{})
}
