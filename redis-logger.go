package logger

import (
	"context"
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"log"
	"os"
)

// RedisWriter 爲 logger 提供寫入 redis 隊列的 io 接口
type RedisWriter struct {
	cli     *redis.Client
	listKey string
}

func (w *RedisWriter) Write(p []byte) (int, error) {
	n, err := w.cli.RPush(context.Background(), w.listKey, p).Result()
	if err != nil {
		log.Printf("write to redis fail %s\n", err)
	}
	return int(n), err
}

// NewRedisLogger use redis client to write log to redis
func NewRedisLogger(client *redis.Client, appName string, option InitOption) *zap.Logger {
	encoder := GetJSONEncoder()
	redisWriter := &RedisWriter{
		cli:     client,
		listKey: "log_list",
	}
	var syncer zapcore.WriteSyncer
	if option.ConsoleLog {
		syncer = zapcore.NewMultiWriteSyncer(
			zapcore.AddSync(os.Stdout),
			zapcore.AddSync(redisWriter),
		)
	} else {
		syncer = zapcore.AddSync(redisWriter)
	}
	core := zapcore.NewCore(encoder, syncer, option.Level)
	return zap.New(core).WithOptions(zap.AddCaller()).With(zap.String("app_name", appName))
}
