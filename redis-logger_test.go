package logger

import (
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"
	"testing"
	"time"
)

func TestNewRedisLogger(t *testing.T) {
	rdb := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})
	redisLogger := NewRedisLogger(rdb, "pair-arbitrage-server", InitOption{
		ConsoleLog: true,
		Level:      zap.DebugLevel,
	})
	if redisLogger == nil {
		t.Error("redis logger is nil")
	}
	redisLogger.Sugar().Infow("test info", AddData(map[string]interface{}{
		"key": 1,
	}))
	time.Sleep(time.Second * 1)
}
